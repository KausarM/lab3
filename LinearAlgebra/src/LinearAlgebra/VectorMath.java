//Kausar Mussa
//1738212

package LinearAlgebra;
public class VectorMath {

	public static void main(String[] args) {
		
		//created three Vector3d objects to test my 3 methods in the Vector3d class
		
		Vector3d north = new Vector3d (4,8,12);
		Vector3d south = new Vector3d (7,12,30);
		Vector3d east = new Vector3d (1,6,11);
	
		//testing the magnitude method while I code
		System.out.println(north.magnitude());
		
		//testing the dotProduct method
		System.out.println(south.dotProduct(east));

		//testing the add method
		Vector3d west = north.add(south);
		System.out.println("(" + west.getX() + " ," + west.getY()+ ", " + west.getZ()+ ")");
}
}