package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
public class Vector3dTest {
	
	@Test
	//test that creates a Vector3d and confirms that the get methods return the correct results
	void testgetMethods() {
		Vector3d w = new Vector3d (22,90,13);
		assertEquals(22, w.getX());
		assertEquals(90, w.getY());
		assertEquals(13, w.getZ());
	}
	@Test
	// test that creates a Vector3d and confirms that the magnitude() method returns the correct result.
	void testmagnitude() {
		Vector3d a = new Vector3d (4,8,12);
		double mag = a.magnitude();
		assertEquals(14.966629547095765, mag);
	}
	
	@Test
	// test that creates two Vector3ds and confirms that the dotProduct() method returns the correct result
	void testdotProduct() {
		Vector3d b = new Vector3d (7,12,30);
		Vector3d c = new Vector3d (1,6,11);
		assertEquals(409.0, b.dotProduct(c));
	}
	@Test 
	// test that creates two Vector3ds and confirms that the add() method returns the correct result.
	void testadd() {
		Vector3d f = new Vector3d (4,8,12);
		Vector3d g = new Vector3d (7,12,30);
		Vector3d h = f.add(g);
		assertEquals(11.0,h.getX());
		assertEquals(20.0,h.getY());
		assertEquals(42.0,h.getZ());
	}

}
