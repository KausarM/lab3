//Kausar Mussa
//1738212

package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
	//initializing variables
	private double x;
	private double y;
	private double z;
	
	//get methods for the 3 variables
	public double getX() {
		return this.x;
	}
	public double getY() {
		return this.y;
	}
	public double getZ() {
		return this.z;
	}
	//constructor
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	//A method magnitude() which returns the magnitude of the this Vector3d
	public double magnitude() {
		double magnitude = Math.sqrt((x*x)+(y*y)+(z*z));
		return magnitude ;
	}
	//A method dotProduct() which should take as input another Vector3d and return a
	//double representing the dot product of the two vectors
	
	public double dotProduct(Vector3d dot) {
	double product = (x * dot.x ) + (y * dot.y )+ (z * dot.z );
		return product;
	}
	//A method add() which should take as input another Vector3d and return a new Vector3d
	//calculated by adding the this Vector3d with the other input
	
	public Vector3d add(Vector3d term) {
		Vector3d adding = new Vector3d((x+term.x),(y+term.y),(z+term.z)) ;
		return adding;
}
}
